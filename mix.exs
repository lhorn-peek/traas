defmodule Traas.MixProject do
  use Mix.Project

  def project do
    [
      app: :traas,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :websockex],
      mod: {Traas, []}
    ]
  end

  defp deps do
    [
      {:websockex, "~> 0.4.0"}
    ]
  end
end
