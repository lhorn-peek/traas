defmodule Traas.TickerTests do
    use ExUnit.Case

    #alias Traas.TickerTests.Subscriber
  
    test "subscriber receives ticks" do
        {:ok, pid} = Traas.TickerTests.Subscriber.start_link()
        
        Traas.TickerTests.Subscriber.subscribe_to_ticker("binance", "btcusdt")
        
        Process.sleep(3500)

        send pid, {:get_ticks, self()}

        assert_receive({:ticks, _}, 1500)
    end

  end
  