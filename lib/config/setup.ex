defmodule Traas.Setup do

    @register [%{module: Traas.Exchange.Binance, id: :binance}]


    def configure_specs, do: Enum.map(@register, fn(p) -> configure(p) end) 

    defp configure(%{} = provider) do
        {_, specs} = 
            provider 
                |> get_info
                |> setup_ticker
                |> setup_order
        specs
    end

    defp get_info(%{module: module}, initial_specs \\ []) do
        {:ok, info} = apply(module, :get_info, [])
        {info, initial_specs}
    end
    
    defp setup_ticker({info, current_specs}) do
        updated_specs = current_specs ++ [{Traas.Ticker.Supervisor, info}]
        {info, updated_specs}
    end

    defp setup_order({info, current_specs}) do
        #TODO: implement when order comp complete

        {info, current_specs}
    end
end