defmodule Traas.Exchange.Binance do
    @behaviour Traas.Exchange.Spec

    alias Traas.Exchange.Info

    @binance %Info{
        exchange: :binance,
        stream_url: "wss://stream.binance.com:9443/ws/",
        fn_symbol_address_mapper: &__MODULE__.ticker_address_from/2
    }

    def get_info() do
        {:ok, @binance}
    end

    def authenticate() do
        {:error, "SPEC NOT IMPLEMENTED!"}
    end

    def ticker_address_from(stream_url, symbol), do: "#{stream_url}#{symbol}@ticker"
end