defmodule Traas do
  @moduledoc false

  use Application
  import Traas.Common.ProcessRegistration, only: [shared_registry_spec: 0]

  alias Traas.Setup

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    
    provider_specs =  
      Setup.configure_specs 
      |> Enum.concat

    children = [shared_registry_spec() | provider_specs]

    opts = [strategy: :one_for_one, name: Traas.Supervisor]
    Supervisor.start_link(children, opts)
  end
end