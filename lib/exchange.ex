defmodule Traas.Exchange do
    
    defmodule Info do 
        defstruct [
            :exchange, 
            :stream_url, 
            :auth_url, 
            :symbols,
            :fn_symbol_address_mapper
        ]
    end

    defmacro __using__(_opts) do
        quote do
        import TestCase

        # Initialize @tests to an empty list
        @tests []

        # Invoke TestCase.__before_compile__/1 before the module is compiled
        @before_compile TestCase
        end
    end
end
