defmodule Traas.TickerTests.Subscriber do
    require Logger
    alias Traas.Ticker.Supervisor, as: TickerSupervisor

    @name :subscriber_abc

    def start_link() do
        {:ok, pid} = Task.start_link(fn -> loop([]) end)
        Process.register(pid, @name)
        {:ok, pid}
    end

    def subscribe_to_ticker(exchange, symbol) do
        subscriber = Process.whereis(@name)
        TickerSupervisor.ticker_subscribe(exchange, {symbol, subscriber})
    end

    defp loop(ticks) do
        receive do
         {:broadcast, message} ->
            latest_ticks = handle_receive({:handle, message}, ticks)
            loop(latest_ticks)
         {:get_ticks, caller} ->
            send caller, {:ticks, ticks}    
            loop(ticks)
        end
    end

    defp handle_receive({:handle, message}, ticks) do
        Logger.info("SUBSCRIBER RECEIVED MESSAGE:#{message}")
        [message | ticks]
    end
end