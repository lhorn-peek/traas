defmodule Traas.Ticker.Hub do
    use WebSockex
    require Logger
    import Traas.Common.ProcessRegistration, only: [via_registry: 1]

    alias Traas.Ticker.Subscriptions
    

    def start_link(name, opts) do
        {:ok, state} = create_initial_state(opts)
        WebSockex.start_link(state.url, __MODULE__, state, [name: via_registry(name)])
    end
    
    defp create_initial_state(%{} = initial_state) do
        {:ok, initial_state}
    end

    def subscribe(hub, {symbol, subscriber}) do
        :ok = WebSockex.cast(via_registry(hub), {:subscribe, symbol, subscriber})
        {:ok, hub, subscriber}
    end

    # CALLBACKS
    def handle_connect(_conn, state) do
        Logger.info("connected to stream!")
        {:ok, state}
    end

    def handle_cast({:subscribe, symbol, subscriber}, state) do
        :ok = Subscriptions.subscribe(state.subscriptions, subscriber)
        {:ok, state}
    end

    def handle_frame({:text, msg}, state) do
        Logger.info "ticker received frame..."

        :ok = Subscriptions.broadcast(state.subscriptions, msg)
        {:ok, state}
    end
end