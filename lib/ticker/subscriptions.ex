defmodule Traas.Ticker.Subscriptions do
    require Logger
    import Traas.Common.ProcessRegistration, only: [new_registry_args: 2]

    @blank_key "_"

    def start_link(name) do
        reg_args = new_registry_args(:duplicate, name)
        Registry.start_link(reg_args)
    end

    def subscribe(subs, subscriber) when is_pid(subscriber) do 
        {:ok, _} = Registry.register(subs, @blank_key, subscriber)
        :ok
    end

    def broadcast(subs, message) do
        Registry.dispatch(subs, @blank_key, fn entries ->
            for {_, subscriber} <- entries, do: send(subscriber, {:broadcast, message})
        end)
        :ok
    end
end