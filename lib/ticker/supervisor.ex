defmodule Traas.Ticker.Supervisor do
    use DynamicSupervisor
    alias Traas.Ticker.Hub
    alias Traas.Exchange.Info, as: Config
    alias Traas.Ticker.Settings.{Specs, IdMap}
    import Traas.Common.ProcessRegistration, only: [via_registry: 1, process_exists?: 1]

    def start_link(%Config{} = config) do
        name = IdMap.supervisor(config.exchange)
        DynamicSupervisor.start_link(__MODULE__, config, name: via_registry(name))
    end

    # API 
    def ticker_subscribe(exchange, {symbol, subscriber}) do
        supervisor = IdMap.supervisor(exchange)
        ticker = IdMap.ticker(exchange, symbol)
        
        unless ticker_started?(ticker) do 
            url = 
                exchange 
                |> IdMap.settings_cache
                |> via_registry
                |> Agent.get(fn config -> config end)
                |> (&(&1.fn_symbol_address_mapper.(&1.stream_url, symbol))).()

            {:ok, _} = start_ticker(supervisor, ticker, {exchange, symbol, url})
        end

        Hub.subscribe(ticker, {symbol, subscriber})
        :ok
    end

    #STARTUP
    def init(%Config{} = config) do
        {:ok, _} = init_cache(config)
        DynamicSupervisor.init(strategy: :one_for_one)
    end

    defp init_cache(%Config{} = config) do
        name = IdMap.settings_cache(config.exchange)
        Agent.start_link(fn -> config end, name: via_registry(name))
    end

    defp start_ticker(supervisor, name, {exchange, symbol, url}) do
        {:ok, subs} = start_subscriptions(supervisor, IdMap.subscriptions(exchange, symbol))

        opts = %{
            :url => url,
            :symbol => symbol,
            :subscriptions => subs
        }
        DynamicSupervisor.start_child(via_registry(supervisor), Specs.ticker(name, opts))
    end

    defp start_subscriptions(supervisor, name) do
        DynamicSupervisor.start_child(via_registry(supervisor), Specs.subscriptions(name))
        {:ok, name}
    end

    defp ticker_started?(ticker) do
        process_exists?(ticker)
    end
end

