defmodule Traas.Ticker.Settings.Config do
    @enforce_keys [ :exchange,:stream_url_base,:base_to_symbol_fn]
    
    defstruct [
        :exchange,
        :stream_url_base,
        :base_to_symbol_fn
    ] 
end