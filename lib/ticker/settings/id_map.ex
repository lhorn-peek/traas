defmodule Traas.Ticker.Settings.IdMap do

    @seperator "_"

    @ticker "tkr"
    def ticker(exchange, symbol) do
        exchange
        |> to_string
        |> prepend_list([@ticker, to_string(symbol)])
        |> Enum.join(@seperator)
        |> String.downcase
    end

    @supervisor "sup"
    def supervisor(exchange) do 
        "#{exchange}#{@seperator}#{@supervisor}#{@seperator}#{@ticker}"
    end    

    @ticker_subs "tickersubs"
    def subscriptions(exchange, symbol) do 
        exchange
        |> to_string
        |> prepend_list([@ticker_subs, to_string(symbol)])
        |> Enum.map(&String.capitalize/1)
        |> Module.concat
    end

    @settings "sett"
    def settings_cache(exchange) do 
        "#{exchange}#{@seperator}#{@ticker}#{@seperator}#{@settings}"
    end  

    defp prepend_list(item, list) when is_list(list), do: [item | list]
end