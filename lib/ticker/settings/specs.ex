defmodule Traas.Ticker.Settings.Specs do
    
    def ticker(name, opts) do
        %{
            id: Traas.Ticker.Hub,
            start: {Traas.Ticker.Hub, :start_link, [name, opts]}
        }
    end

    def subscriptions(name) do
        %{
            id: Traas.Ticker.Subscriptions,
            start: {Traas.Ticker.Subscriptions, :start_link, [name]}
        }
    end
end