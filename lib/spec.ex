defmodule Traas.Exchange.Spec do

    @callback get_info() :: {:ok, Info.t} | {:error, String.t}

    @callback authenticate() :: {:ok, String.t} | {:error, String.t}

    @callback ticker_address_from(String.t, String.t) :: {:ok, String.t} | {:error, String.t}
end