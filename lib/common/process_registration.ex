defmodule Traas.Common.ProcessRegistration do
    
    # SHARED REGISTRY

    @shared_registry Traas.ProcessRegistry

    def shared_registry_spec(), do: {Registry, [keys: :unique, name: @shared_registry]}

    def via_registry(process_name), do: {:via, Registry, {@shared_registry, process_name}}

    # GENERAL REGISTRY HELPERS

    def new_registry_args(name), do: [keys: :unique, name: name]

    def new_registry_args(:duplicate, name), do: [keys: :duplicate, name: name]

    def process_exists?(process_name) do
        case Registry.lookup(@shared_registry, process_name) do
            []         -> false
            [{_, _}]   -> true   
        end
    end
end