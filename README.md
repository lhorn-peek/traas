# Traas

Trading assistant(traas) is a client library for connecting to external crypto exchanges, such as binance etc.

The idea is that traas will be the core package for a bigger vision of creating a crypto bot framework using Elixir and OTP.

At the moment, traas can only connect to a market pair feed from an exchange, and push the data received from that feed/exchange to local subscribers(other otp processes) in a app. This way subscribers dont need to connect to the web socket feed themselves, thus eliminating the need for many connections for the same data. This is where the bigger vision of a custom bot framework comes in(more on this in the future).

## ToDo:
- connect to exchange socket feed for a certain pair using 1 exchange, ie: "Binance btc-usdt". ---> COMPLETE

- implement pub sub for local subscriber processes to subscribe to a Traas ticker. ---> COMPLETE

- add "exchange" component to Traas, ie: actors/supervision tree. This will be used for authenticating a user account as well as a wrapper over the "order" component for placing buy/sell orders on an exchange (see below).

- add "order" component to Traas. Handles the actual buying and selling of assets.

- test and plan next development cycle. 

## Getting Started

- clone the repo

- run `mix deps.get` command in a console, within the traas git directory.

- run `mix compile`

- run `mix test` to see passing test.

(optional) to see it working from subscriber point of view execute the following:
- in a console, run `iex -S mix`
- type in `Traas.TickerTests.Subscriber.start_link()`, hit enter
- type in `Traas.TickerTests.Subscriber.subscribe_to_ticker("binance", "btcusdt")`

### Prerequisites

Elixir, Erlang OTP installed on your local dev machine. see https://elixir-lang.org/ to get started.


## Running the tests

Within the traas git repo/directory on your local machine, type:
`mix compile` then `mix test`


## Authors

* **LHORN**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

